import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, IonRouterOutlet, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { timer } from 'rxjs';
import { Storage } from '@ionic/storage';
//import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChild(IonRouterOutlet, {static: false}) routerOutlet: IonRouterOutlet;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private nav: NavController,
    private router: Router,
    private storage: Storage,
    private alertController: AlertController,
    //private ga: GoogleAnalytics
  ) {
    this.initializeApp();
    this.platform.backButton.subscribe(() => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else {
        this.confirmClose();
      }
    });
  }

  async confirmClose(){
    const alert = await this.alertController.create({
        message: 'Are you sure you want to close this app?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            cssClass: 'dark',
            handler: (blah) => {
              //console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Yes',
            handler: () => {
              navigator['app'].exitApp();
            }
          }
        ]
      });

      await alert.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      Promise.all([this.storage.get("loggedin")
      ]).then(values => {
        if (values[0]) {
            this.nav.navigateRoot('tabs/tab1');
        }
        else{
          this.nav.navigateRoot('welcome');
        }
      });  
      this.splashScreen.hide();
      /*var notificationOpenedCallback = function(jsonData) {
        //alert('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        //swal.fire(JSON.stringify(jsonData));
        //this.nav.navigateForward('/notifications');
        this.router.navigateByUrl('/notifications');
      };
  
      window["plugins"].OneSignal
        .startInit("696dc764-5d12-4f07-9d29-434f20a95675", "16672518016")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

      this.ga.startTrackerWithId('UA-175870767-1')
      .then(() => {}).catch(e => alert('Error starting GoogleAnalytics == '+ e));*/
    });
  }
}
