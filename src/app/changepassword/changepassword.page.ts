import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangepasswordPage implements OnInit {

  @ViewChild('curpass', { static: true }) curpass;
  @ViewChild('newpass', { static: true }) newpass;
  @ViewChild('confirmpass', { static: true }) confirmpass;
  error;
  ishidden: boolean;

  constructor(public apiService: ApiService, private storage: Storage) { }

  async ngOnInit() {
  	this.ishidden = true;
  }

  signup(){
  	this.error = '';
  	if (this.curpass.value == '' || this.confirmpass.value == '' || this.newpass.value == '' ) {
      this.error = 'Please enter values for all fields';
      return;
    }
    else if (this.curpass.value.length < 6 || this.confirmpass.value.length < 6 || this.newpass.value.length < 6) {
      this.error = 'Password cannot be less than 6 characters';
      return;
    }   
    else if (this.confirmpass.value != this.newpass.value) {
      this.error = 'New password and confirmation password do not match';
      return;
    }
    else {
	    this.ishidden = false;
		const uploadData = new FormData();
		uploadData.append('curpass', this.curpass.value);
		uploadData.append('password', this.newpass.value);
		this.apiService.createItem(uploadData, 'changepassword').subscribe((response) => {
			this.ishidden = true;
			if (response['status'] == 1) {
		        this.apiService.presentAlert('Successful', 'Password updated successfully');
			}
			else{
				this.apiService.presentAlert('Failed', JSON.stringify(response['message']));
			}
		});
	}
  }

}
