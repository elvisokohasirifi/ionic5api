import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditprofilePage implements OnInit {

  @ViewChild('name', { static: true }) name;
  @ViewChild('email', { static: true }) email;
  @ViewChild('phone', { static: true }) phone;
  @ViewChild('password', { static: true }) password;
  error;
  ishidden: boolean;

  constructor(public apiService: ApiService, private storage: Storage) { }

  async ngOnInit() {
  	this.ishidden = true;
  	this.name.value = this.apiService.name;
  	this.email.value = this.apiService.email;
  	this.phone.value = this.apiService.phone;
  	//alert(this.apiService.access_token);
  }

  signup(){
  	this.error = '';
  	if (this.name.value == '') {
      this.error = 'Please enter a valid name';
      return;
    }
    else if (this.name.value.length < 3 || !this.name.value.includes(' ')) {
      this.error = 'Please enter your full name';
      return;
    }   
    else if (this.email.value == '') {
      this.error = 'Please enter a valid email';
      return;
    }
    else if (this.phone.value == null) {
      this.error = 'Please enter a valid phone number';
      return;
    }
    else {
	    this.ishidden = false;
		const uploadData = new FormData();
		uploadData.append('email', this.email.value);
		uploadData.append('phone', this.phone.value);
		uploadData.append('name', this.name.value);
		this.apiService.createItem(uploadData, 'editaccount').subscribe((response) => {
			this.ishidden = true;
			if (response['status'] == 1) {
		        this.apiService.save('name', this.name.value);
		        this.apiService.save('email', this.email.value);
		        this.apiService.save('phone', this.phone.value);
		        this.apiService.name = this.name.value;
		        this.apiService.email = this.email.value;
		        this.apiService.phone = this.phone.value;
		        this.apiService.presentAlert('Successful', 'Profile updated successfully');
			}
			else{
				this.apiService.presentAlert('Failed', JSON.stringify(response['message']));
			}
		});
	}
  }

}
