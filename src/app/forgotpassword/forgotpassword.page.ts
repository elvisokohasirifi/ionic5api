import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {
  @ViewChild('email', { static: true }) email;
  ishidden: boolean;
  error;
  constructor(public apiService: ApiService) { }

  ngOnInit() {
  	this.ishidden = true;
  }

  signup(){
  	this.apiService.navigate('signup');
  }

  submit(){
  	this.error = '';
  	if (this.email.value == '') {
     this.error = "Please enter your email address";
      return;
    }
    else {
    	this.ishidden = false;
		const uploadData = new FormData();
		uploadData.append('email', this.email.value);
		this.apiService.createItem(uploadData, 'forgotpassword').subscribe((response) => {
			this.ishidden = true;
			if (response['status'] == 1) {
				// set storage values
				this.apiService.navigate('setpassword');
			}
			else{
				this.apiService.presentAlert('Failed', response['message']);
			}
		});
    }
  }

}
