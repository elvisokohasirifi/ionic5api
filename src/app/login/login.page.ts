import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('email', { static: true }) email;
  @ViewChild('password', { static: true }) password;
  error;
  ishidden: boolean;

  constructor(public apiService: ApiService) { }

  ngOnInit() {
  	this.ishidden = true;
  }

  signup(){
  	this.apiService.navigate('signup');
  }

  login(){
  	this.error = '';
  	if (this.email.value == '') {
     this.error = "Please enter your email address";
      return;
    }
    else if (this.password.value.length < 6) {
      this.error = "Password must have at least 6 characters";
      return;
    }
    else if (this.password.value == '') {
      this.error = "Please enter your password";
      return;
    }
    else {
    	/*this.oneSignal.getIds().then((id) => {
        	this.send(this.email.value, this.password.value, id['userId']);
      	});*/
      //this.send(this.email.value, this.password.value, '1234567890');
      this.ishidden = false;
  		const uploadData = new FormData();
  		uploadData.append('email', this.email.value);
  		uploadData.append('password', this.password.value);
  		//console.log(uploadData);
  		this.apiService.login(uploadData, 'login').subscribe((response) => {
  			this.ishidden = true;
  			if (response['status'] == 1) {
          this.apiService.save('name', response['user']['name']);
          this.apiService.save('email', response['user']['email']);
          this.apiService.save('phone', response['user']['phone']);
          this.apiService.save('token', response['access_token']);
          this.apiService.name = response['user']['name'];
          this.apiService.email = response['user']['email'];
          this.apiService.phone = response['user']['phone'];
          this.apiService.access_token = response['access_token'];
          this.apiService.save('loggedin', true);
  				this.apiService.home('tabs/tab1');
  			}
  			else{
  				this.apiService.presentAlert('Failed', JSON.stringify(response['message']));
  			}
  		});
    }
  }

  forgotpassword(){
  	this.apiService.navigate('forgotpassword');
  }

}
