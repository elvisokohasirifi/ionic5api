import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(public apiService: ApiService) { }

  ngOnInit() {
  }

  navigate(url){
  	this.apiService.navigate(url);
  }

  logout(){
  	if (!confirm('Are you sure you want to log out?')) {
  		return;
  	}
  	this.apiService.createItem(null, 'logout').subscribe((response) => {
  		
  	});
  	this.apiService.clear();
  	this.apiService.home('welcome');
  }
}
