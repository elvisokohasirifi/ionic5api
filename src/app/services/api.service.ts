import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { NavController, ToastController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // API path
  //base_path = 'http://127.0.0.1:8000/api/';
  base_path = 'http://shareridegh.herokuapp.com/api/';
  access_token = '';
  name;
  email;
  phone;
  httpOptions;

  constructor(private http: HttpClient, private nav: NavController, private alertController: AlertController, private storage: Storage) {
    Promise.all([this.storage.get("name"), this.storage.get("email"), this.storage.get("phone"), this.storage.get('token')]).then(values => {    
      this.name = values[0];
      this.email = values[1];
      this.phone = values[2];
      this.access_token = values[3];
      // Http Options
      this.httpOptions = {
        headers: new HttpHeaders({
            'Authorization': 'Bearer ' + values[3]
        })
      }
    });
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      alert('Network Error ' + error.error.message);
    } else {
      alert(JSON.stringify(error.error));
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };


  // Create a new object
  createItem(formdata, path): Observable<any> {
    let headers = {
        'Authorization': 'Bearer ' + this.access_token
    };
    return this.http
      .post<any>(this.base_path + path, formdata, {headers: headers})
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get single object by ID
  getItem(path): Observable<any> {
    return this.http
      .get<any>(this.base_path + path, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get all objects data
  getList(path): Observable<any> {
    return this.http
      .get<any>(this.base_path + path, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Update item by id
  updateItem(formdata, path): Observable<any> {
    return this.http
      .put<any>(this.base_path + path, formdata, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Delete item by id
  deleteItem(formdata, path) {
    return this.http
      .delete<any>(this.base_path + path, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // login object
  login(formdata, path): Observable<any> {
    return this.http
      .post<any>(this.base_path + path, formdata, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  navigate(url){
  	this.nav.navigateForward(url);
  }

  back(){
  	this.nav.pop();
  }

  home(url){
    this.nav.navigateRoot(url);
  }

  async presentAlert(title, message) {
    const alert = await this.alertController.create({
      subHeader: title,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  save(key, value){
    this.storage.set(key, value);
  }
  
  async get(key){
    let value = null;
    await this.storage.get(key).then((val) => {
      value = val;
    });
    return value;
  }

  clear(){
    this.storage.clear();
    this.name = null;
    this.email = null;
    this.access_token = null;
    this.phone = null;
  }

}
