import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-setpassword',
  templateUrl: './setpassword.page.html',
  styleUrls: ['./setpassword.page.scss'],
})
export class SetpasswordPage implements OnInit {

  @ViewChild('token', { static: true }) token;
  @ViewChild('password', { static: true }) password;
  error;
  ishidden: boolean;

  constructor(public apiService: ApiService) { }

  ngOnInit() {
  	this.ishidden = true;
  }

  goback(){
  	this.apiService.back();
  }

  login(){
  	this.error = '';
  	if (this.token.value == '') {
     this.error = "Please enter the token";
      return;
    }
    else if (this.password.value.length < 6) {
      this.error = "Password must have at least 6 characters";
      return;
    }
    else if (this.password.value == '') {
      this.error = "Please enter your password";
      return;
    }
    else {
      	this.ishidden = false;
		const uploadData = new FormData();
		uploadData.append('token', this.token.value);
		uploadData.append('password', this.password.value);
		this.apiService.createItem(uploadData, 'setpassword').subscribe((response) => {
			this.ishidden = true;
			if (response['status'] == 1) {
				// set storage values
				this.apiService.presentAlert('Success', 'Password reset successful. Log in to continue.');
				this.apiService.navigate('login');
			}
			else{
				this.apiService.presentAlert('Failed', response['message']);
			}
		});
    }
  }

}
