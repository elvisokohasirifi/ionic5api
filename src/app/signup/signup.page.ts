import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  @ViewChild('name', { static: true }) name;
  @ViewChild('email', { static: true }) email;
  @ViewChild('phone', { static: true }) phone;
  @ViewChild('password', { static: true }) password;
  error;
  ishidden: boolean;

  constructor(public apiService: ApiService) { }

  ngOnInit() {
  	this.ishidden = true;
  }

  login(){
  	this.apiService.back();
  }

  signup(){
  	this.error = '';
  	if (this.name.value == '') {
      this.error = 'Please enter a valid name';
      return;
    }
    else if (this.name.value.length < 3 || !this.name.value.includes(' ')) {
      this.error = 'Please enter your full name';
      return;
    }   
    else if (this.email.value == '') {
      this.error = 'Please enter a valid email';
      return;
    }
    else if (this.phone.value == null) {
      this.error = 'Please enter a valid phone number';
      return;
    }
    else if (this.password.value == '') {
      this.error = 'Please enter a valid password';
      return;
    }
    else if (this.password.value.length < 6) {
      this.error = 'Password must have at least 6 characters';
      return;
    }
    else {
	    	/*this.oneSignal.getIds().then((id) => {
	      		this.signupuser(this.email.value, this.password.value, this.phone.value, this.name.value, this.region.value, id['userId']);
	      	});*/
	    this.ishidden = false;
		const uploadData = new FormData();
		uploadData.append('email', this.email.value);
		uploadData.append('password', this.password.value);
		uploadData.append('phone', this.phone.value);
		uploadData.append('name', this.name.value);
		this.apiService.createItem(uploadData, 'register').subscribe((response) => {
			this.ishidden = true;
			if (response['status'] == 1) {
        this.apiService.save('name', response['user']['name']);
        this.apiService.save('email', response['user']['email']);
        this.apiService.save('phone', response['user']['phone']);
        this.apiService.save('token', response['access_token']);
        this.apiService.name = response['user']['name'];
          this.apiService.email = response['user']['email'];
          this.apiService.phone = response['user']['phone'];
          this.apiService.access_token = response['access_token'];
        this.apiService.save('loggedin', true);
				this.apiService.home('tabs/tab1');
			}
			else{
				this.apiService.presentAlert('Failed', JSON.stringify(response['message']));
			}
		});
	}
  }

}
