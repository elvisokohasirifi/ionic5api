import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../services/api.service';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  @ViewChild(IonSlides, { static:true } ) slides: IonSlides;
  num;
  constructor(public apiService: ApiService) { }

  ngOnInit() {
    //this.ga.trackView('Welcome');
    this.num = 1;
  }

  next(){
    //this.storage.set('role', role);
    //this.nav.navigateForward('login');
    if (this.num == 3) {
    	this.apiService.home('login');
    }
    else{
    	this.num++;
    	this.slides.slideNext();
    }
  }

}
